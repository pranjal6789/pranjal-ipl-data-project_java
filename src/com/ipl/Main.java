package com.ipl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {
    public static final int ID = 0;
    public static final int SEASON = 1;
    public static final int WINNER = 10;
    public static final int MATCHID = 0;
    public static final int BOWLINGTEAM = 3;
    public static final int BATTINGTEAM = 2;
    public static final int EXTRARUNS = 16;
    public static final int TOTALRUNS = 17;
    public static final int BOWLERS = 8;
    public static final int BATSMAN = 6;
    public static final int BATSMANRUN = 15;
    public static final int PLAYEROFMATCH = 13;
    public static final int TOSSWINNER = 6;
    public static final int NOBALL = 13;
    public static final int DISMISSALKIND = 17;

    public static void main(String[] args) {
        List<Match> matches = getMatchesData();
        List<Delivery> deliveries = getDeliveryData();
        findNumberOfMatchesPlayedPerYear(matches);
        findNumberOfMatchesWinByEachTeam(matches);
        findExtraRun2016PerTeam(matches, deliveries);
        findEconomyOfBowlersIn2015(matches, deliveries);
        findRunScoredByDhoniIn2015(matches, deliveries);
        findRunScoredByKohliIn2016(matches, deliveries);
        numberOfPlayerOfMatchWinbyDeviliers(matches);
        numberofTossWinByMumbaiIndians(matches);
        numberOfNoBallBowledByDaleSteyn(deliveries);
        numberOfRunsMadeByRCB(deliveries);
    }

    public static List<Match> getMatchesData() {
        List<Match> matches = new ArrayList();
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("./src/data/matches.csv"));
            br.readLine();
            while ((line = br.readLine()) != null) {
                Match match = new Match();
                String[] matchData = line.split(",");
                match.setId(Integer.parseInt(matchData[ID]));
                match.setSeason(Integer.parseInt(matchData[SEASON]));
                match.setWinner(matchData[WINNER]);
                match.setPlayerOfMatch(matchData[PLAYEROFMATCH]);
                match.setTossWinner((matchData[TOSSWINNER]));
                matches.add(match);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matches;
    }

    public static List<Delivery> getDeliveryData() {
        List<Delivery> deliveries = new ArrayList();
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("./src/data/deliveries.csv"));
            br.readLine();
            while ((line = br.readLine()) != null) {
                Delivery delivery = new Delivery();
                String[] deliveryData = line.split(",");
                delivery.setMatchId(Integer.parseInt(deliveryData[MATCHID]));
                delivery.setBowlingTeam(deliveryData[BOWLINGTEAM]);
                delivery.setExtraRuns(Integer.parseInt(deliveryData[EXTRARUNS]));
                delivery.setBowlerName(deliveryData[BOWLERS]);
                delivery.setTotalRuns(Integer.parseInt(deliveryData[TOTALRUNS]));
                delivery.setBatsmanName(deliveryData[BATSMAN]);
                delivery.setBatsmanRuns(Integer.parseInt(deliveryData[BATSMANRUN]));
                delivery.setNoBall(Integer.parseInt(deliveryData[NOBALL]));
                delivery.setDismissalKind(deliveryData[DISMISSALKIND]);
                delivery.setBattingTeam(deliveryData[BATTINGTEAM]);
                deliveries.add(delivery);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveries;
    }

    public static void findNumberOfMatchesPlayedPerYear(List<Match> matches) {
        HashMap<Integer, Integer> totalOfMatchesPlayedPerYear
                = new HashMap<Integer, Integer>();
        for (int i = 0; i < matches.size(); i++) {
            Match match = matches.get(i);
            int season = match.getSeason();
            if (totalOfMatchesPlayedPerYear.containsKey(season)) {
                totalOfMatchesPlayedPerYear.put(season, totalOfMatchesPlayedPerYear.get(season) + 1);
            } else {
                totalOfMatchesPlayedPerYear.put(season, 1);
            }
        }
        System.out.println(totalOfMatchesPlayedPerYear);
    }

    public static void findNumberOfMatchesWinByEachTeam(List<Match> matches) {
        HashMap<String, Integer> numberOfMatchesWinByEachTeam
                = new HashMap<String, Integer>();
        for (int i = 0; i < matches.size(); i++) {
            Match match = matches.get(i);
            String winner = match.getWinner();
            if (!winner.equals("")) {
                if (numberOfMatchesWinByEachTeam.containsKey(winner)) {
                    numberOfMatchesWinByEachTeam.put(winner, numberOfMatchesWinByEachTeam.get(winner) + 1);
                } else {
                    numberOfMatchesWinByEachTeam.put(winner, 1);
                }
            }
        }
        System.out.println(numberOfMatchesWinByEachTeam);
    }

    public static void findExtraRun2016PerTeam(List<Match> matches, List<Delivery> deliveries) {
        HashMap<String, Integer> extraRunPerTeam
                = new HashMap<String, Integer>();
        for (int i = 0; i < matches.size(); i++) {
            Match match = matches.get(i);
            int season = match.getSeason();
            int matchId = match.getId();
            if (season == 2016) {
                for (int j = 0; j < deliveries.size(); j++) {
                    Delivery delivery = deliveries.get(j);
                    int deliveryId = delivery.getMatchId();
                    int extraRun = delivery.getExtraRuns();
                    if (deliveryId == matchId) {
                        String team = delivery.getBowlingTeam();
                        if (extraRunPerTeam.containsKey(team)) {
                            extraRunPerTeam.put(team, extraRunPerTeam.get(team) + extraRun);
                        } else {
                            extraRunPerTeam.put(team, 0);
                        }
                    }
                }
            }
        }
        System.out.println(extraRunPerTeam);
    }

    public static void findEconomyOfBowlersIn2015(List<Match> matches, List<Delivery> deliveries) {
        HashMap<String, Float> economyOfEachBowler
                = new HashMap<>();
        HashMap<String, Integer> totalRun
                = new HashMap<String, Integer>();
        HashMap<String, Integer> totalBall
                = new HashMap<String, Integer>();
        for (int i = 0; i < matches.size(); i++) {
            Match match = matches.get(i);
            int season = match.getSeason();
            if (season == 2015) {
                int id = match.getId();
                for (int j = 0; j < deliveries.size(); j++) {
                    Delivery delivery = deliveries.get(j);
                    int deliveryId = delivery.getMatchId();
                    int totalRuns = delivery.getTotalRuns();
                    if (id == deliveryId) {
                        String bowlerName = delivery.getBowlerName();
                        if (totalRun.containsKey(bowlerName)) {
                            totalRun.put(bowlerName, totalRun.get(bowlerName) + totalRuns);
                        } else {
                            totalRun.put(bowlerName, 1);
                        }
                        if (totalBall.containsKey(bowlerName)) {
                            totalBall.put(bowlerName, totalBall.get(bowlerName) + 1);
                        } else {
                            totalBall.put(bowlerName, 0);
                        }
                    }
                }
            }
        }
        totalRun.forEach((key, value) -> {
            if (totalBall.containsKey(key)) {
                economyOfEachBowler.put(key, (float) (value / (float) ((totalBall.get(key) / 6))));
            }
        });
        System.out.println(economyOfEachBowler);
    }

    public static void findRunScoredByDhoniIn2015(List<Match> matches, List<Delivery> deliveries) {
        HashMap<String, Integer> totalRunScored
                = new HashMap<>();
        int totalRunByDhoni = 0;
        for (int i = 0; i < matches.size(); i++) {
            Match match = matches.get(i);
            int season = match.getSeason();
            if (season == 2012) {
                int id = match.getId();
                for (int j = 0; j < deliveries.size(); j++) {
                    Delivery delivery = deliveries.get(j);
                    int deliveryId = delivery.getMatchId();
                    if (id == deliveryId) {
                        String batsman = delivery.getBatsmanName();
                        if (batsman.equals("MS Dhoni")) {
                            int batsmanRun = delivery.getBatsmanRuns();
                            totalRunByDhoni += batsmanRun;
                            totalRunScored.put(batsman, totalRunByDhoni);
                        }
                    }
                }
            }
        }
        System.out.println(totalRunScored);
    }

    public static void findRunScoredByKohliIn2016(List<Match> matches, List<Delivery> deliveries) {
        HashMap<String, Integer> totalRunScoredByKohli
                = new HashMap<>();
        int totalRunByKohli = 0;

        for (int i = 0; i < matches.size(); i++) {
            Match match = matches.get(i);
            int season = match.getSeason();
            if (season == 2016) {
                int id = match.getId();
                for (int j = 0; j < deliveries.size(); j++) {
                    Delivery delivery = deliveries.get(j);
                    int delivery_id = delivery.getMatchId();
                    if (id == delivery_id) {
                        String batsman = delivery.getBatsmanName();
                        if (batsman.equals("V Kohli")) {
                            int batsmanRun = delivery.getBatsmanRuns();
                            totalRunByKohli += batsmanRun;
                            totalRunScoredByKohli.put(batsman, totalRunByKohli);
                        }
                    }
                }
            }
        }
        System.out.println(totalRunScoredByKohli);
    }

    public static void numberOfPlayerOfMatchWinbyDeviliers(List<Match> matches_data) {
        HashMap<String, Integer> totalPlayerOfMatch = new HashMap<>();
        int totalWinnerOfPlayerOfMatch = 0;
        for (int i = 0; i < matches_data.size(); i++) {
            Match match = matches_data.get(i);
            String playerOfMatch = match.getPlayerOfMatch();
            if (playerOfMatch.equals("AB de Villiers")) {
                totalWinnerOfPlayerOfMatch += 1;
//                    System.out.println(totalWinnerOfPlayerOfMatch);
                totalPlayerOfMatch.put("AB de Villiers", totalWinnerOfPlayerOfMatch);
            }
        }
        System.out.println(totalPlayerOfMatch);
    }

    public static void numberofTossWinByMumbaiIndians(List<Match> matches) {
        HashMap<String, Integer> totalTossWinByMumbai = new HashMap<>();
        int totalTossWinByMI = 0;
        for (int i = 0; i < matches.size(); i++) {
            Match match = matches.get(i);
            String tossWinner = match.getTossWinner();
            if (tossWinner.equals("Mumbai Indians")) {
                totalTossWinByMI += 1;
                totalTossWinByMumbai.put("Mumbai Indians", totalTossWinByMI);
            }
        }
        System.out.println(totalTossWinByMumbai);
    }

    public static void numberOfNoBallBowledByDaleSteyn(List<Delivery> deliveries) {
        HashMap<String, Integer> totalNoBallByDaleSteyn = new HashMap<>();
        int totalNoBalls = 0;
        for (int i = 0; i < deliveries.size(); i++) {
            Delivery delivery = deliveries.get(i);
            String bowler = delivery.getBowlerName();
            if (bowler.equals("DW Steyn")) {
                int noBall = delivery.getNoBall();
                if (noBall != 0) {
                    totalNoBalls += 1;
                    totalNoBallByDaleSteyn.put("DW Steyn", totalNoBalls);
                }
            }
        }
        System.out.println(totalNoBallByDaleSteyn);
    }

    public static void numberOfRunsMadeByRCB(List<Delivery> deliveries) {
        HashMap<String, Integer> totalRunsByRCB = new HashMap<>();
        int totalRuns = 0;
        for (int i = 0; i < deliveries.size(); i++) {
            Delivery delivery = deliveries.get(i);
            String team = delivery.getBattingTeam();
            if (team.equals("Royal Challengers Bangalore")) {
                int run = delivery.getTotalRuns();
                totalRuns += run;
                totalRunsByRCB.put("Royal Challengers Bangalore", totalRuns);
            }
        }
        System.out.println(totalRunsByRCB);
    }
}
